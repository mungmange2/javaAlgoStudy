package example.test;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TestServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, IllegalStateException {
        System.out.println("Hello, Servlet!, HttpServletRequest 와 HttpServletResponse 반드시 있어야 함!");
        PrintWriter out = response.getWriter();
        out.print("<p>Hello!!</p>");
    }
}
