package stack;

import java.util.ArrayList;
import java.util.List;

public class StringDecoding {
    public static void main(String[] args) {
        String data = "3[a4[c]]"; // -> a4[c]a4[c]a4[c]..acccacccaccc
        //String data = "3[a]z"; // -> aaa..aaaz

        int i = 0;
        String result = "";
        if (Character.getNumericValue(data.charAt(i)) >= 0 && Character.getNumericValue(data.charAt(i)) <= 9) { // 숫자다.
            data = StringDecoding.replaceString(data, i);
            System.out.println("result>>" + data);
        }
    }

    public static String replaceString(String data, int i) {
        String result = "";
        int num = Character.getNumericValue(data.charAt(i)); // 숫자 값 저장
        List<String> text = new ArrayList<>(); // 스택 생성
        for (int j=0; j<num-1; j++) { // 숫자 만큼 반복 a4[c]
            int t = i+1;
            while (data.charAt(t) != 93) {
                text.add(Character.toString(data.charAt(++t)));
            }
            for (int s = 0; s<text.size(); s++) {
                result += text.get(s);
            }
        }
        return result;
    }
}
