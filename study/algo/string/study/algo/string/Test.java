
public class Test {
    public static void main(String[] args) {
        /**
         * [1, 2, 1] -> [1, 2, 2] -> [1, 2, 3]
         * [1, 2, 3] -> [3, 2, 1] -> [1, 3, 2] -> [2, 3, 1]
         * [2, 1, 4, 4] -> [2, 1, 3, 4]
         * */
    }

    public int solution(int N) {
        if (N == 1) {
            return 0;
        }
        int result = 1;
        for (int i=0; i<(int)(Math.log10(N)); i++) {
            result *= 10;
        }
        return result;
    }
}
