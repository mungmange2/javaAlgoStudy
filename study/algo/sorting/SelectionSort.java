package study.algo.sorting;

import java.util.Arrays;

public class SelectionSort {
    /*
    * 배열 순서 중 가장 작은 수를 찾아 열의 왼쪽 끝에 있는 숫자와 교환
    *
    * 1. 배열의 값 중 제일 작은 수를 찾는다.
    * 2. 찾은 수의 위치를 파악 후 맨 왼쪽 값과 위치를 교환
    * */
    public static void main(String[] args) {
        int arr[] = {3, 7, 9, 5, 2, 1, 4, 6, 8};
        for (int i=0; i<arr.length - 1; i++) {
            int min = arr[i];
            int minLocation = i;
            for(int j=i; j<arr.length - 1; j++) {
                if (min > arr[j]) {
                    min = arr[j];
                    minLocation = j;
                }
            }
            int temp = arr[i];
            arr[i] = arr[minLocation];
            arr[minLocation] = temp;
            System.out.println(Arrays.toString(arr));
        }
        System.out.println(Arrays.toString(arr));
    }
}
