package study.algo.sorting;

import java.util.Arrays;

public class BubbleSort {
    /*
        앞 뒤 수를 비교하여 앞이 뒤보다 큰 경우 위치를 바꾸어 정렬하는 알고리즘
    * */
    public static void main(String[] args) {
        int[] arr = {5, 9, 3, 1, 2, 8, 4, 7, 6};
        for (int i=0; i < arr.length-1; i++) {
            for (int j=0; j < arr.length-1; j++) {
                if (arr[j] > arr[j+1]) {
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }
}