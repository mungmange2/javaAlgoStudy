package study.algo.array;

import java.util.Arrays;

public class TwoSum {

    public static void main(String[] args) {
        /**
         * input:
         *   nums = [2, 7, 11, 15], target = 9
         * output:
         *   [0, 1]
         */
        System.out.println(Arrays.toString(두수의_합(26)));
        System.out.println(Arrays.toString(두수의_합_By_차감(26)));
        System.out.println(Arrays.toString(두수의_합_By_포인터이동(26)));
    }

    public static int[] 두수의_합(int target) {
        int[] nums = {2, 7, 11, 15};
        int[] result = new int[2];
        int nextI = 1;
        for (int i=0; i<nums.length; i++) {
            if (nums[i] + nums[nextI] == target) {
                result[0] = i;
                result[1] = nextI;
                break;
            }
            nextI += 1;
        }
        return result;
    }

    public static int[] 두수의_합_By_차감(int target) {
        int[] nums = {2, 7, 11, 15};
        int[] result = new int[2];
        int nextI = 1;
        for (int i=0; i<nums.length; i++) {
            if (target - nums[i] == nums[nextI]) {
                result[0] = i;
                result[1] = nextI;
                break;
            }
            nextI += 1;
        }
        return result;
    }

    public static int[] 두수의_합_By_포인터이동(int target) {
        int[] nums = {2, 7, 11, 15};
        int left = 0;
        int right = nums.length - 1;
        int[] result = new int[2];
        while(left != right) {
            if (nums[left] + nums[right] < target) {
                left += 1;
            } else if (nums[left] + nums[right] > target) {
                right -= 1;
            } else {
                result[0] = left;
                result[1] = right;
                break;
            }
        }
        return result;
    }
}
