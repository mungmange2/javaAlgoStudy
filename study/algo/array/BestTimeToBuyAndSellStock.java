package study.algo.array;

public class BestTimeToBuyAndSellStock {

    /**
     * input: [7, 1, 5, 3, 6, 4]
     * output: 5
     *
     * -> 1일때 사서 6일때 팔면 5의 이득을 얻음
     *
     * input: [2, 4, 1]
     * output: 2
     *
     * 1. 최소수를 구함
     * 2. 최소값 기준, 배열의 값 - 최소수 차감한 값을 result 와 비교하여 큰 값을 result 에 담음
     * 3. 반복
     * */
    public static void main(String[] args) {
        int[] input = {7, 1, 5, 3, 6, 4};
        int min = Integer.MAX_VALUE;
        int result = 0;
        for (int i=0; i<input.length; i++) {
            if (input[i] < min) { // 최소값 구하기
                min = input[i];
            }
            System.out.println("result:" + result +", "+ "input[i]:" + (input[i] - min));
            result = Math.max(result, input[i] - min);
        }
        System.out.println(result);
    }
}
