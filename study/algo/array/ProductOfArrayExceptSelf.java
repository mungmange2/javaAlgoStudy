package study.algo.array;

import java.util.Arrays;

public class ProductOfArrayExceptSelf {

    public static void main(String[] args) {
        /**
         * input: [1,2,3,4]
         * output: [24, 12, 8, 6]
         *
         * input[i] 을 제외한 모든 결과의 곱을 output 에 담기
         * */
        ProductOfArrayExceptSelf.solution1();
        ProductOfArrayExceptSelf.solution2();
    }

    public static void solution1() {
        int[] input = {-1, 1, 1, -1};
        int[] output = new int[input.length];
        for (int i=0; i<input.length; i++) {
            int result = 1;
            for (int j=0; j<input.length; j++) {
                if (i == j) {
                    continue;
                }
                result *= input[j];
            }
            output[i] = result;
        }

        System.out.println(Arrays.toString(output));
    }

    public static void solution2() {
        int[] input = {1, 2, 3, 4};
        int[] output = new int[input.length];
        int result = 1;
        for (int i=0; i<input.length; i++) {
            output[i] = result;
            result *= input[i];
        }
        // output: {1, 1, 2, 6}
        // input: {4, 3, 2, 1}
        result = 1;
        for(int i = input.length - 1; i >=0; i--) {
            output[i] *= result; // 결과 만드는 곳
            //System.out.println("output["+i+"]::" + output[i]);
            //System.out.println("result::" + result);
            result *= input[i]; // 그 다음 번 준비
            //System.out.println("result::" + result);
        }
        System.out.println(Arrays.toString(output));
    }
}
