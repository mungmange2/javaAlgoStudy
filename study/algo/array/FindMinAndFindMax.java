package study.algo.array;

public class FindMinAndFindMax {
    /**
     * input: [7, 1, 5, 3, 6, 4]
     * output: 1, 7
     * */
    public static void main(String[] args) {
        정렬안하고_제일작은수와제일큰수찾기();
    }

    public static void 정렬안하고_제일작은수와제일큰수찾기() {
//        int[] prices = {7, 5, 1, 3, 6, 4};
        int[] prices = {6, 7, 4, 1, 2, 3, 5};
        int min = prices[0];
        int max = prices[0];
        int i=0;
        while(i < prices.length-1) {
            if (max < prices[i+1]) {
                max = prices[i+1];
            }
            if (min >= prices[i+1]) {
                min = prices[i];
            }
            i++;
        }
        System.out.println(min + ", " + max);
    }
}
