package study.logic;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class VacationDay {

    public static void main(String[] args) {
        int workingDays = VacationDay.vacationDay();
        System.out.println("휴가일: " + workingDays);
    }

    /**
     * 시작일 을 반복문 을 통해 더하 면서 주말 일에 해당 되는지 체크 하여 날짜 계산
     * */
    public static int vacationDay() {
        LocalDate startDate = LocalDate.of(2021, 06, 26);
        LocalDate endDate = LocalDate.of(2021, 06, 27);
        int vacationDay = 1; // 연차 기준일

        while (startDate.isBefore(endDate)) { // 시작 일이 종료 일보다 크다. break
            DayOfWeek startDayOfWeek = startDate.getDayOfWeek(); // 시작일 주말 값
            if (startDayOfWeek == DayOfWeek.SATURDAY || startDayOfWeek == DayOfWeek.SUNDAY) { // 시작일 주말이 토요일 or 일요일 인 경우 continue
                startDate = startDate.plusDays(1); // 시작일 + 1
                continue;
            }
            startDate = startDate.plusDays(1); // 시작일 + 1
            ++vacationDay; // 휴가일 + 1
        }
        return vacationDay;
    }
}
