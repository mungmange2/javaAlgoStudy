package com.example.api.events;

import java.net.URI;

import org.modelmapper.ModelMapper;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RequestMapping(value = "/api/events", produces = MediaTypes.HAL_JSON_VALUE)
@Controller
public class EventController {

    private final EventRepository eventRepository;
    private final ModelMapper modelMapper;
    private final EventVaildator eventVaildator;

    //@Autowired 생성자가 하나만 있고, 받아온 파라미터가 빈으로 등록되어 있으면 Autowired 빼도댐 (스프링 4.3 부터)
    public EventController(EventRepository eventRepository, ModelMapper modelMapper, EventVaildator eventVaildator) {
        this.eventRepository = eventRepository;
        this.modelMapper = modelMapper;
        this.eventVaildator = eventVaildator;
    }

    @PostMapping
    public ResponseEntity createEvent(@RequestBody @Valid EventDto eventDto, Errors errors) {
        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(errors);
        }

        // 디테일한 에러 검증
        this.eventVaildator.validator(eventDto, errors);
        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(errors);
        }

        // modelMapper
        // Event.class 인스턴스로 만들어 달라!
        Event event = this.modelMapper.map(eventDto, Event.class);
        Event newEvent = this.eventRepository.save(event); // 저장이 된 객체가 나옴

        // linkTo, methodOn -> HATEOS 에서 제공
        // uri 생성
        URI createUri = linkTo(EventController.class).slash(newEvent.getId()).toUri();
        return ResponseEntity.created(createUri).body(event); // uri 만들어서 201 상태 리턴 및 body 리턴
    }
}
