package com.example.api.events;

import java.time.LocalDateTime;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder // 내가 입력하는 값이 무엇인지 알수있다.
@AllArgsConstructor @NoArgsConstructor
@Getter @Setter
@EqualsAndHashCode(of = "id") // entity 간의 연관관계 생성 시 상호 참조에 의해 stackoverflow 발생소지 있음, 특정 값만으로 비교하도록 하는것 (of)
//@Data <-- 상호 참조에 의해 stackoverflow 발생소지 있음.
@Entity
public class Event {

    @Id @GeneratedValue
    private Integer id;
    private String name;
    private String description;
    private LocalDateTime beginEnrollmentDateTime;
    private LocalDateTime closeEnrollmentDateTime;
    private LocalDateTime beginEventDateTime;
    private LocalDateTime endEventDateTime;
    private String location; // (optional) 이게 없으면 온라인 모임
    private int basePrice; //(optional)
    private int maxPrice; // (optional)
    private int limitOfEnrollment;
    private boolean offline;
    private boolean free;
    @Enumerated(EnumType.STRING) // 기본값은 EnumType.ORDINAL (순서값에 따라 숫자로 저장됨..)
    private EventStatus eventStatus;
}
