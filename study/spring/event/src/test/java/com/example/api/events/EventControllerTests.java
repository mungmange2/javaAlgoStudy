package com.example.api.events;

import java.time.LocalDateTime;

import com.example.api.common.TestDescription;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

@RunWith(SpringRunner.class)
//@WebMvcTest //MockMvc 빈을 자동 설정 해줌. 웹 관련 빈만 등록해 줌.
@SpringBootTest
@AutoConfigureMockMvc
public class EventControllerTests {
    @Autowired
    private MockMvc mockmvc; // 스프링 MVC 테스트 핵심 클래스, dispatcher servlet 상대로 가짜 요청과 응답을 보내고 확인 가능.

    @Autowired
    private ObjectMapper objectMapper; // objectMapper 은 BeanSerializer 에 등록된 객체임

    //@MockBean
    //EventRepository eventRepository;

    @Test
    @TestDescription("정상적으로 이벤트를 생성")
    public void createEvent() throws Exception {
        Event event = Event.builder()
                .name("Spring")
                .description("REST API TEST")
                .beginEnrollmentDateTime(LocalDateTime.of(2021, 1, 1, 1, 1, 1))
                .closeEnrollmentDateTime(LocalDateTime.of(2021,12,31,1,1,1))
                .basePrice(100)
                .maxPrice(200)
                .location("서울역")
                .eventStatus(EventStatus.PUBLISHED)
                .build();

        //event.setId(10);
        //  Event event = this.modelMapper.map(eventDto, Event.class); 을 적용하지 않아 아래의 로직에서 null error 남
        //Mockito.when(this.eventRepository.save(event)).thenReturn(event);// 저장 후(when) 호출이 되면 event을 리턴하라

        this.mockmvc.perform(post("/api/events/")
                .contentType("application/json")
                .accept(MediaTypes.HAL_JSON) // 받고싶은 타입.
                .content(objectMapper.writeValueAsString(event))
        ).andDo(print()).andExpect(status().isCreated())
                .andExpect(header().exists(HttpHeaders.LOCATION))
                //.andExpect(jsonPath("id").exists() // path에 id가 있는지 확인
                .andExpect(jsonPath("id").value(Matchers.not(100)) // value 가 맞는지 확인

        );
    }

    @Test
    @TestDescription("입력 받을 수 없는 값을 사용한 경우")
    public void createEvent_Bad_Request_Empty_Input() throws Exception {
        //EventDto eventDto = EventDto.builder().build();
        Event eventDto = Event.builder()
                .name("Spring")
                .description("REST API ERROR TEST")
                .beginEnrollmentDateTime(LocalDateTime.of(2021, 11, 26, 1, 1, 1))
                .closeEnrollmentDateTime(LocalDateTime.of(2021,11,25,1,1,1))
                .beginEventDateTime(LocalDateTime.of(2021, 11, 24, 1, 1, 1))
                .endEventDateTime(LocalDateTime.of(2021, 11, 23, 1, 1, 1))
                .basePrice(100)
                .maxPrice(200)
                .location("서울역")
                .eventStatus(EventStatus.PUBLISHED)
                .build();

        this.mockmvc.perform(post("/api/events")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.objectMapper.writeValueAsString(eventDto)))
                .andExpect(jsonPath("$[0].objectName").exists())
                .andExpect(jsonPath("$[0].field").exists())
                .andExpect(jsonPath("$[0].defaultMessage").exists())
                .andExpect(jsonPath("$[0].code").exists())
                .andExpect(jsonPath("$[0].rejectedValue").exists())
                .andDo(print()).andExpect(status().isBadRequest());
    }
}
