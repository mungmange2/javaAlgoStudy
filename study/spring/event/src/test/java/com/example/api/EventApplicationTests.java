package com.example.api;

import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

import com.example.api.events.Event;
import org.junit.jupiter.api.Test;

// cmd + shift + t: @SpringBootTest 테스트 생성
// ctrl + shift + t: 테스트 실행
// ctrl + r: 이전 실행 했던 것 실행
@SpringBootTest
class EventApplicationTests {

    @Test
    public void builder() {
        Event event = Event.builder()
                .name("하이헬로우")
                .description("설명!")
                .build();
        assertThat(event).isNotNull();
    }

    @Test
    public void javaBean() {
        Event event = new Event(); //@Builder 만 사용할 경우 안됨 ( @AllArgsConstructor @NoArgsConstructor 적용 필요 )
        event.setName("헬로네임");
        assertThat(event.getName()).isEqualTo("헬로네임");
    }
}
