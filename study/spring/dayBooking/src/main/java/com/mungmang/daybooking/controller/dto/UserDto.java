package com.mungmang.daybooking.controller.dto;

import lombok.Data;

public class UserDto {

    @Data
    public static class UserRequest {
        private String signId;
        private String userName;
        private String password;
    }
}
