package com.mungmang.daybooking.controller;

import com.mungmang.daybooking.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;


@Slf4j
@Controller
@RequiredArgsConstructor
public class ViewController {

    private final UserService userService;

    /**
     * 메인 화면
     * */
    @GetMapping("/")
    public String index(Model model) {
        String userId = this.userService.getUserId();
        model.addAttribute("userId", userId);
        return "/index";
    }

    /**
     * 회원가입 화면
     * */
    @GetMapping("/join")
    public String join() {
        return "/user/join";
    }

    /**
     * 로그인 화면
     * */
    @GetMapping("/login")
    public String login() {
        return "/user/login";
    }

    /**
     * 로그인 실패
     * */
    @PostMapping("/login/fail")
    public String loginFail(HttpServletRequest request, Model model) {
        model.addAttribute("message", request.getAttribute("message"));
        return "/user/login";
    }

    /**
     * 날짜 예약 관리 화면 진입
     * */
    @GetMapping("/day")
    public String day(Model model) {
        String userId = this.userService.getUserId();
        if ("error".equals(userId)) {
            return "/user/denied";
        }
        model.addAttribute("userId", userId);
        return "/user/day";
    }

    /**
     * 날짜 예약 신청 화면 진입
     * */
    @GetMapping("/booking")
    public String DayBookingRequest(Model model) {
        String userId = this.userService.getUserId();
        if ("error".equals(userId)) {
            return "/user/denied";
        }
        model.addAttribute("userId", userId);
        return "/user/booking";
    }

    /**
     * 로그아웃 결과 화면
     * */
    @GetMapping("/logout")
    public String logout() {
        return "/user/logout";
    }

    /**
     * 접근 거부 페이지
     * */
    @GetMapping("/denied")
    public String dispDenied() {
        return "/user/denied";
    }
}