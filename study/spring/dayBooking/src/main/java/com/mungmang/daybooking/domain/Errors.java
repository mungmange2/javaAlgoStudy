package com.mungmang.daybooking.domain;

import lombok.Getter;

@Getter
public enum Errors {
    USER_NOT_FOUND(-0000, "사용자 정보를 찾을 수 없습니다."),
    USED_DAY_BOOKING_ZERO_ERROR(-0001, "날짜 설정 값은 0일 보다 커야합니다."),
    DAY_BOOKING_FAILED(-0002, "신청할 날짜가 존재하지 않습니다."),
    DAY_BOOKING_DELETE_FAILED(-0003, "삭제할 날짜가 존재하지 않습니다."),
    DAY_BOOKING_DATE_ERROR(-0004, "시작일자가 종료일자보다 클 수 없습니다."),
    DAY_BOOKING_NOW_DATE_ERROR(-0005, "시작일자가 오늘날짜보다 작을 수 없습니다."),
    DAY_BOOKING_CREATE_ERROR(-0006, "이미 등록된 날짜가 있습니다."),
    DAY_BOOKING_CREATE_ERROR_HOLIDAY(-0007, "휴일에는 날짜를 등록할 수 없습니다.");

    private int code;
    private String message;

    Errors(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
