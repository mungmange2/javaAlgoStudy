package com.mungmang.daybooking.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.FieldError;

@Getter
@Setter
public class BindingException extends RuntimeException {

    private FieldError fieldError;
    private String message;

    public BindingException(FieldError e) {
        this.fieldError = e;
        this.message = e.getDefaultMessage();
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setFieldError(FieldError fieldError) {
        this.fieldError = fieldError;
    }
}
