package com.mungmang.daybooking.controller.dto;

import lombok.Data;
import java.util.Date;
import java.util.List;

public class DayBookingDto {

    @Data
    public static class DayBookingResponse {
        private String userName; // 사용자 명
        private BookingInfo bookingInfo; // 날짜 예약 정보
    }

    @Data
    public static class BookingInfo {
        private Integer remainDate; // 남은 일자
        private List<UsedDates> usedDates; // 사용 날짜 예약정보 리스트

    }

    @Data
    public static class UsedDates {
        private Long idx;
        private Date startDate; // 시작일
        private Date endDate; // 종료일
        private Integer duration; // 예약 날짜 기간
        private String comment; // 코멘트
    }

    @Data
    public static class CreateDayBookingRequest {
        private Date startDate; // 시작일
        private Date endDate; // 종료일
        private String comment; // 코멘트
    }
}
