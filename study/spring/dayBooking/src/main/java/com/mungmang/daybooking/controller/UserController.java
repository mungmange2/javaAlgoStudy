package com.mungmang.daybooking.controller;

import com.mungmang.daybooking.controller.dto.UserDto;
import com.mungmang.daybooking.entity.User;
import com.mungmang.daybooking.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

@Slf4j
@Controller
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    /**
     * 사용자 회원 가입
     * */
    @PostMapping(value = "")
    public RedirectView saveUser(UserDto.UserRequest userRequest) {
        User user = this.userService.saveUser(userRequest);
        if (!ObjectUtils.isEmpty(user)) {
            return new RedirectView("/login");
        }
        return new RedirectView("/index");
    }
}
