package com.mungmang.daybooking.service;

import com.mungmang.daybooking.controller.dto.DayBookingDto;
import com.mungmang.daybooking.domain.Errors;
import com.mungmang.daybooking.entity.User;
import com.mungmang.daybooking.entity.DayBooking;
import com.mungmang.daybooking.exception.CommonException;
import com.mungmang.daybooking.repository.UserRepository;
import com.mungmang.daybooking.repository.DayBookingRepository;
import com.mungmang.daybooking.util.DateUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.transaction.Transactional;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class DayBookingService {

    private final UserRepository userRepository;
    private final DayBookingRepository dayBookingRepository;
    private final static Integer defaultDay = 30;

    /**
     * 남은 예약 날짜 계산
     * */
    public Integer calculateRemain(List<DayBooking> DayBookingList) {
        Integer remain = 0;
        for (DayBooking DayBooking : DayBookingList) {
            remain += DayBooking.getDuration();
        }
        return defaultDay - remain;
    }

    /**
     * 날짜 조회
     * */
    public DayBookingDto.DayBookingResponse getUserDayBooking(String userId) {

        // 사용자 정보 조회
        User user = this.userRepository.findAllByUserId(userId);
        if (ObjectUtils.isEmpty(user)) {
            throw new CommonException(Errors.USER_NOT_FOUND);
        }

        // 예약 날짜 내역 리스트 조회
        List<DayBooking> DayBookings = this.dayBookingRepository.findAllByUserIdAndIsCanceledFalseOrderByStartDateDesc(userId);
        DayBookingDto.BookingInfo DayBookingInfo = new DayBookingDto.BookingInfo();

        // 사용자 예약 날짜 내역 상세 조회
        if (!ObjectUtils.isEmpty(DayBookings)) {
            DayBookingInfo.setRemainDate(this.calculateRemain(DayBookings)); // 남은일
            List<DayBookingDto.UsedDates> usedDates = new ArrayList<>();

            // 예약 날짜 내역 리스트 담기
            for (DayBooking DayBooking : DayBookings) {
                DayBookingDto.UsedDates dates = new DayBookingDto.UsedDates();
                dates.setIdx(DayBooking.getIdx());
                dates.setStartDate(DateUtil.localDateTimeToDate(DayBooking.getStartDate()));
                dates.setEndDate(DateUtil.localDateTimeToDate(DayBooking.getEndDate()));
                dates.setDuration(DayBooking.getDuration());
                dates.setComment(DayBooking.getComment());
                usedDates.add(dates);
            }
            DayBookingInfo.setUsedDates(usedDates);
        // 초기 범위 30일로 지정
        } else {
            DayBookingInfo.setRemainDate(30);
        }
        DayBookingDto.DayBookingResponse userDayBookingResponse = new DayBookingDto.DayBookingResponse();
        userDayBookingResponse.setUserName(user.getUsername());
        userDayBookingResponse.setBookingInfo(DayBookingInfo);
        return userDayBookingResponse;
    }

    /**
     * 날짜 예약
     * */
    public DayBooking createUserDayBooking(String userId, DayBookingDto.CreateDayBookingRequest createDayBookingRequest) {

        // 사용자 날짜 사용 내역 리스트 조회
        Integer remain = defaultDay;
        List<DayBooking> DayBookings = this.dayBookingRepository.findAllByUserIdAndIsCanceledFalseOrderByStartDateDesc(userId);
        if (!ObjectUtils.isEmpty(DayBookings)) {
            remain = this.calculateRemain(DayBookings); // 기준날짜 - 사용한날짜 = 남은 날짜
        }
        if (remain <= 0) {
            throw new CommonException(Errors.DAY_BOOKING_FAILED);
        }

        // 요청값: 시작일과 종료일
        LocalDateTime startDate = DateUtil.dateToLocalDateTime(createDayBookingRequest.getStartDate());
        LocalDateTime endDate = DateUtil.dateToLocalDateTime(createDayBookingRequest.getEndDate());

        // 이미 등록한 예약날짜가 존재하는지 확인
        for (DayBooking dayBooking : DayBookings) {
            // 등록 되지 말아야 할 조건: startDate < dbEndDate || endDate > dbStart
            if ((startDate.compareTo(dayBooking.getEndDate()) < 0) && (endDate.compareTo(dayBooking.getStartDate()) > 0)) {
                throw new CommonException(Errors.DAY_BOOKING_CREATE_ERROR);
            }
        }

        // 날짜 계산
        Integer duration = 1;
        duration = this.getUsedDay(startDate, endDate, duration);

        // 예약할 날짜가 없는 경우
        if (duration > remain) {
            throw new CommonException(Errors.DAY_BOOKING_FAILED);
        }

        // 날짜 예약 신청일이 0일 인 경우 (그럴일은 없겠지만)
        if (ObjectUtils.isEmpty(duration) || duration < 0.0f) {
            throw new CommonException(Errors.USED_DAY_BOOKING_ZERO_ERROR);
        }

        DayBooking dayBooking = DayBooking.builder()
                .userId(userId)
                .duration(duration)
                .comment(createDayBookingRequest.getComment())
                .startDate(DateUtil.dateToLocalDateTime(createDayBookingRequest.getStartDate()))
                .endDate(DateUtil.dateToLocalDateTime(createDayBookingRequest.getEndDate()))
                .isCanceled(false)
                .regDate(LocalDateTime.now()).build();
        return this.dayBookingRepository.saveAndFlush(dayBooking);
    }

    /**
     * 예약 날짜 삭제
     * */
    public void deleteDayBooking(String userId, Long idx) {
        List<DayBooking> dayBookings = this.dayBookingRepository.findAllByUserIdAndIdxAndIsCanceledFalse(userId, idx);
        if (ObjectUtils.isEmpty(dayBookings)) {
            throw new CommonException(Errors.DAY_BOOKING_DELETE_FAILED);
        }
        DayBooking dayBooking = DayBooking.builder()
                .idx(idx)
                .userId(userId)
                .isCanceled(true)
                .cancelDate(LocalDateTime.now()).build();
        this.dayBookingRepository.save(dayBooking);
    }

    /**
     * 사용된 날짜 계산 (주말 제외)
     * */
    public static Integer getUsedDay(LocalDateTime startDate, LocalDateTime endDate, Integer duration) {

        // 시작일 > 종료일
        if (startDate.isAfter(endDate)) {
            throw new CommonException(Errors.DAY_BOOKING_DATE_ERROR);
        }

        // 시작날짜가 오늘날짜보다 이전인 경우
        if (ChronoUnit.DAYS.between(LocalDateTime.now(), startDate) < 0) {
            throw new CommonException(Errors.DAY_BOOKING_NOW_DATE_ERROR);
        }

        // 토요일 ~ 일요일 등록할 경우
        if (startDate.getDayOfWeek() == DayOfWeek.SATURDAY && endDate.getDayOfWeek() == DayOfWeek.SUNDAY) {
            throw new CommonException(Errors.DAY_BOOKING_CREATE_ERROR_HOLIDAY);
        }

        // 주말 제외 날짜 계산
        LocalDate start = startDate.toLocalDate();
        LocalDate end = endDate.toLocalDate();

        while (start.isBefore(end)) { // 시작 일이 종료 일보다 크다. break
            start = start.plusDays(1); // 시작일 + 1
            DayOfWeek startDayOfWeek = start.getDayOfWeek(); // 시작일 주말 값
            if (startDayOfWeek == DayOfWeek.SATURDAY || startDayOfWeek == DayOfWeek.SUNDAY) { // 시작일 주말이 토요일 or 일요일 인 경우 continue
                continue;
            }
            ++duration; // 날짜 + 1
        }
        return duration;
    }
}
