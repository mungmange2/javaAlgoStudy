package com.mungmang.daybooking.entity;

import com.mungmang.daybooking.domain.Grade;
import lombok.*;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Entity
public class User {
    @Id
    private String userId;
    private String signId;
    private String username;
    private String password;
    @Enumerated(EnumType.STRING)
    private Grade grade;
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime joinDate;

    public void update(String signId, String username, String password) {
        this.signId = signId;
        this.username = username;
        this.password = password;
        this.grade = Grade.GENERAL;
        this.joinDate = LocalDateTime.now();
    }
}