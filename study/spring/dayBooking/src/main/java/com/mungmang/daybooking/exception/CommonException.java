package com.mungmang.daybooking.exception;

import com.mungmang.daybooking.domain.Errors;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CommonException extends RuntimeException {

    private int code;
    private String message;

    public CommonException(Errors e) {
        this.code = e.getCode();
        this.message = e.getMessage();
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

