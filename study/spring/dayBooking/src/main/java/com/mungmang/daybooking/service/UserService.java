package com.mungmang.daybooking.service;

import com.mungmang.daybooking.controller.dto.UserDto;
import com.mungmang.daybooking.domain.Errors;
import com.mungmang.daybooking.domain.Grade;
import com.mungmang.daybooking.entity.User;
import com.mungmang.daybooking.exception.CommonException;
import com.mungmang.daybooking.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.time.LocalDateTime;
import java.util.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.userRepository.findAllBySignId(username);
        if (ObjectUtils.isEmpty(user)) {
            throw new UsernameNotFoundException(Errors.USER_NOT_FOUND.getMessage());
        }
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(Grade.GENERAL.name()));
        return new org.springframework.security.core.userdetails.User(user.getUserId(), user.getPassword(), authorities);
    }

    /**
     * 회원 가입
     * */
    public User saveUser(UserDto.UserRequest userRequest) {
        User userInfo = this.userRepository.findAllBySignId(userRequest.getSignId());
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        if (!ObjectUtils.isEmpty(userInfo)) { // 방어 로직 (원래는 에러를 일으켜야 하는데 테스트를 위해 해당 로직 작성
            userInfo.update(userInfo.getSignId(), userInfo.getUsername(), passwordEncoder.encode(userRequest.getPassword()));
            return this.userRepository.saveAndFlush(userInfo);
        }
        User user = User.builder()
                .userId(String.valueOf(UUID.randomUUID()).toUpperCase())
                .signId(userRequest.getSignId())
                .username(userRequest.getUserName())
                .password(passwordEncoder.encode(userRequest.getPassword()))
                .grade(Grade.GENERAL)
                .joinDate(LocalDateTime.now())
                .build();
        return this.userRepository.saveAndFlush(user);
    }

    /**
     * 사용자 userId 리턴
     * */
    public String getUserId() {
        Object userInfo = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (ObjectUtils.isEmpty(userInfo) || "anonymousUser".equals(userInfo)) {
            return "error";
        }
        UserDetails userDetails = (UserDetails) userInfo;
        return userDetails.getUsername();
    }
}
