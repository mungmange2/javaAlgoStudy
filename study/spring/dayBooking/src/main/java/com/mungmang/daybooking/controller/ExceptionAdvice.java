package com.mungmang.daybooking.controller;

import com.mungmang.daybooking.domain.Errors;
import com.mungmang.daybooking.exception.BindingException;
import com.mungmang.daybooking.exception.CommonException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@ControllerAdvice
@RestController
public class ExceptionAdvice {

    @ExceptionHandler(CommonException.class)
    public ResponseEntity<CommonException> exception(Errors e) {
        CommonException errorResponse = new CommonException(e);
        errorResponse.setCode(e.getCode());
        errorResponse.setMessage(e.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(BindingException.class)
    public ResponseEntity<BindingException> bindingException(FieldError e) {
        BindingException bindingException = new BindingException(e);
        bindingException.setMessage(e.getDefaultMessage());
        bindingException.setFieldError(e);
        return new ResponseEntity<>(bindingException, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
