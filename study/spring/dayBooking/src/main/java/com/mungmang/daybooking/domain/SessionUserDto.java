package com.mungmang.daybooking.domain;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class SessionUserDto implements Serializable {
    private String userId;
    private String signId;
    private String username;
    private String password;
    private Grade grade;
    private LocalDateTime joinDate;
}
