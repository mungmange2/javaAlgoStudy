package com.mungmang.daybooking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DayBookingApplication {

    public static void main(String[] args) {
        SpringApplication.run(DayBookingApplication.class, args);
    }

}
