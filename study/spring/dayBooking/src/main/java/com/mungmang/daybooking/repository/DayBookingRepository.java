package com.mungmang.daybooking.repository;

import com.mungmang.daybooking.entity.DayBooking;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DayBookingRepository extends JpaRepository<DayBooking, Long> {
    List<DayBooking> findAllByUserIdAndIsCanceledFalseOrderByStartDateDesc(String userId);
    List<DayBooking> findAllByUserIdAndIdxAndIsCanceledFalse(String userId, Long idx);
}
