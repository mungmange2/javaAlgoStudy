package com.mungmang.daybooking.repository;

import com.mungmang.daybooking.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
    User findAllBySignId(String signId);
    User findAllByUserId(String userId);
}
