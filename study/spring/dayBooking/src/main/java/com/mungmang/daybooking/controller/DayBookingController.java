package com.mungmang.daybooking.controller;

import com.mungmang.daybooking.controller.dto.DayBookingDto;
import com.mungmang.daybooking.exception.BindingException;
import com.mungmang.daybooking.service.DayBookingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/day")
@RequiredArgsConstructor
public class DayBookingController {

    private final DayBookingService dayBookingService;

    /**
     * 날짜 예약 정보
     * */
    @GetMapping(value = "/{userId}/booking")
    public ResponseEntity<DayBookingDto.DayBookingResponse> getUserDayBooking(
            @PathVariable(value="userId") String userId) {
        DayBookingDto.DayBookingResponse dayBookingResponse = this.dayBookingService.getUserDayBooking(userId);
        return new ResponseEntity<>(dayBookingResponse, HttpStatus.OK);
    }

    /**
     * 날짜 예약 신청
     * */
    @PostMapping(value = "/{userId}/booking", consumes="application/json")
    public ResponseEntity<Void> createUserDayBooking(
            @PathVariable(value="userId") String userId,
            @RequestBody @Valid DayBookingDto.CreateDayBookingRequest createDayBookingRequest,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new BindingException(bindingResult.getFieldError());
        }
        this.dayBookingService.createUserDayBooking(userId, createDayBookingRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /**
     * 날짜 예약 취소
     * */
    @DeleteMapping(value = "/{userId}/booking/{idx}")
    public ResponseEntity<Void> deleteUserDayBooking(
            @PathVariable(value="userId") String userId,
            @PathVariable(value="idx") Long idx) {
        this.dayBookingService.deleteDayBooking(userId, idx);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
