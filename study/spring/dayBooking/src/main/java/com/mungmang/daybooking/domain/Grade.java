package com.mungmang.daybooking.domain;

import lombok.Getter;

@Getter
public enum Grade {
    GENERAL, ADMIN
}
