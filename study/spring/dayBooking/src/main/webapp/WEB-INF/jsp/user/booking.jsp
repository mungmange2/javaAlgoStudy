<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:import url="../common/header.jsp" />
<script>
    let userId = `${userId}`;
</script>
<div>
    <div class="container">
        <div style="text-align: center">
            <h1>예약 페이지 입니다.</h1>
            <hr>
            <div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <div class="form-group">
                    시작 날짜: <input type="text" id="startDate" class="form-control" style="display: inline;width: 300px;">
                </div>
                <div class="form-group">
                    종료 날짜: <input type="text" id="endDate" class="form-control" style="display: inline;width: 300px;">
                </div>
                <div class="form-group">
                    코멘트: <textarea id="comment" class="form-control" style="display: inline;width: 500px;"></textarea>
                </div>
                <div class="form-group">
                    <button type="button" id="day_regist" class="btn btn-primary">예약 신청</button>
                </div>
            </div>
        </div>
    </div>
</div>
<c:import url="../common/footer.jsp" />