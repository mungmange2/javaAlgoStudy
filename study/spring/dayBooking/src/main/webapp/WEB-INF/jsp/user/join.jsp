<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:import url="../common/header.jsp" />
<div>
    <div class="container">
        <div style="text-align: center">
            <h1>회원가입</h1>
            <hr>
            <form action="/users" method="post" id="join_form">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <div class="form-group">
                    아이디: <input style="width: 300px;display: inline;" class="form-control" type="text" name="signId" placeholder="사용자 아이디를 입력해 주세요.">
                </div>
                <div class="form-group">
                    비밀번호: <input style="width: 300px;display: inline;" class="form-control" type="password" name="password" placeholder="사용자 비밀번호를 입력해 주세요.">
                </div>
                <div class="form-group">
                    사용자명: <input style="width: 300px;display: inline;" class="form-control" type="text" name="userName" placeholder="사용자 명을 입력해 주세요.">
                </div>
                <div class="form-group">
                    <button class="btn btn-info right" type="submit" name="join">회원가입</button>
                </div>
            </form>
        </div>
    </div>
</div>
<c:import url="../common/footer.jsp" />
