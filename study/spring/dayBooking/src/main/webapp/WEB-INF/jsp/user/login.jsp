<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:import url="../common/header.jsp" />
<div>
    <div class="container">
        <div style="text-align: center">
            <h1>로그인</h1>
            <hr>
            <form action="/login" method="post" id="login_form">
                <div class="form-group">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <input class="form-control" type="text" name="username" placeholder="사용자 아이디를 입력해 주세요.">
                    <input class="form-control" type="password" name="password" placeholder="사용자 비밀번호를 입력해 주세요.">
                    <p style="color: red;"> ${message} </p>
                    <button class="btn btn-info right" type="submit" id="login">로그인</button>
                    <button class="btn btn-warning right" type="button" id="join">회원가입</button>
                </div>
            </form>
        </div>
    </div>
</div>
<c:import url="../common/footer.jsp" />
