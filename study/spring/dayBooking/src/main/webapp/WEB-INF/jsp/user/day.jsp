<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<c:import url="../common/header.jsp" />
<script>
    let userId = `${userId}`;
</script>
    <div>
        <div class="container">
            <div style="text-align: center">
                <h1>[<span id="username"></span>] 님 날짜 예약 정보 입니다. <button class="btn btn-danger" id="logout">로그아웃</button></h1>
                <hr>
                <div>
                    <div style="float:right"><button class="btn btn-success" id="dayBookingRequest">날짜 예약 신청</button></div>
                    <div style="float:left;margin-bottom: 10px;">* 남은 날짜: <span id="remainDate"></span> 일</div>

                    <table class="table table-hover" style="margin-top: 10px;">
                        <thead>
                            <th>시작일</th>
                            <th>종료일</th>
                            <th>신청기간</th>
                            <th>코멘트</th>
                            <th>날짜예약취소</th>
                        </thead>
                        <tbody id="usedDatesTbody"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<c:import url="../common/footer.jsp" />