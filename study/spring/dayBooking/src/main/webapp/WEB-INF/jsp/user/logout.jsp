<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<c:import url="../common/header.jsp" />
<div>
    <div class="container">
        <div style="text-align: center">
            <h3>로그아웃 처리되었습니다.</h3>
            <a href="/">메인으로 이동</a>
        </div>
    </div>
</div>
<c:import url="../common/footer.jsp" />