<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<http auto-config="true" use-expressions="true">
<c:import url="common/header.jsp" />
<div>
    <div class="container">
        <div style="text-align: center">
            <h3>날짜 예약 시스템:D-! </h3>
            <sec:authorize access="isAnonymous()">
                <button class="btn btn-warning" id="join">회원가입</button>
                <button class="btn btn-info" id="login">로그인</button>
            </sec:authorize>

            <sec:authorize access="isAuthenticated()">
                <button class="btn btn-danger" id="logout">로그아웃</button>
                <button class="btn btn-primary" id="dayBooking">날짜 예약 메뉴이동</button>
            </sec:authorize>
        </div>
    </div>
</div>
<c:import url="common/footer.jsp" />