$(document).ready(function(){

    $('#halfType, #fourHalfType').hide();

    $("#startDate, #endDate").datepicker({
        dateFormat: 'yy-mm-dd',
        monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
        dayNamesMin: ['일','월','화','수','목','금','토'],
        weekHeader: 'Wk',
        changeMonth: true,
        changeYear: true,
        yearRange:'1900:+0',
        showMonthAfterYear: true,
        buttonImageOnly: true,
        buttonText: '날짜를 선택하세요'
    });

    // 회원가입
    $('#join').on('click', function(){
        location.href = "/join";
    });

    // 로그인 화면 요청
    $('#login').on('click', function(){
        location.href = "/login";
    });

    // 로그아웃
    $('#logout').on('click', function(){
        location.href = "/logout";
    });

    // 예약 관리 화면으로 이동
    $('#dayBooking').on('click', function() {
        location.href = "/day";
    });

    // 예약 페이지로 이동
    $('#dayBookingRequest').on('click', function(){
        if ($('#remainDate').html() === 0) {
            alert('신청할 예약날짜가 존재하지 않습니다.');
            return;
        }
        location.href=`/booking`;
    });

    // 등록 신청 페이지
    if (location.href.includes("/booking")) {
        dayBooking();

    } else if (location.href.includes("/day")) {
        loadDayBooking();
    }

    $(document).on('click', '.deleteDayBooking', function(){
        if (confirm("정말로 삭제하시겠습니까?")) {
            let token = $("meta[name='_csrf']").attr("content");
            let header = $("meta[name='_csrf_header']").attr("content");
            let id = $(this).attr('id');
            let idx = id.split('_')[1];

            $.ajax({
                url: `/day/${userId}/booking/${idx}`,
                type: 'DELETE',
                contentType: "application/json",
                beforeSend : function(xhr) {
                    xhr.setRequestHeader(header, token);
                },
                success: function () {
                    alert('삭제가 완료 되었습니다.');
                    location.href = `/day`;
                },
                error: function(error) {
                    console.log(error);
                    alert(error.responseJSON.message);
                }
            });
            return;
        }
    });
});

// 예약 신청
function dayBooking() {

    $('#day_regist').on('click', function(){
        let token = $("meta[name='_csrf']").attr("content");
        let header = $("meta[name='_csrf_header']").attr("content");
        let data = {};
        data.startDate = $('#startDate').val();
        data.endDate = $('#endDate').val();
        if (![null, undefined, ''].includes($('#comment').val())) {
            data.comment = $('#comment').val();
        }
        $.ajax({
            url: `/day/${userId}/booking`,
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify(data),
            beforeSend : function(xhr) {
                xhr.setRequestHeader(header, token);
            },
            success: function () {
                alert('등록이 완료 되었습니다.');
                location.href = `/day`;
            },
            error: function(error) {
                console.log(error);
                alert(error.responseJSON.message);
            }
        });
    });
}

// 사용자 날짜 예약 내역
function loadDayBooking() {
    $.ajax({
        url: `/day/${userId}/booking`,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            console.log(response);
            const dayBookingInfo = response.bookingInfo;
            $('#username').html(response.userName);
            $('#remainDate').html(dayBookingInfo.remainDate);
            if (![null, '', undefined].includes(dayBookingInfo.usedDates)) {
                const usedDates = dayBookingInfo.usedDates;
                let tags = '';
                for (let dataInfo in usedDates) {
                    const data = usedDates[dataInfo];
                    const comment = (data['comment'] === null) ? '-' : data['comment'];
                    tags += '<tr>';
                    tags += '<td>' + moment(data['startDate']).format('YYYY-MM-DD HH:mm:ss') + '</td>'
                    tags += '<td>' + moment(data['endDate']).format('YYYY-MM-DD HH:mm:ss') + '</td>'
                    tags += '<td>' + data['duration'] + ' 일</td>'
                    tags += '<td>' + comment + '</td>';
                    if (moment().diff(moment(data['startDate']), 'days') <= 0) {
                        const idx = data['idx'];
                        let id = "deleteDayBooking_" + idx;
                        tags += '<td><button id="'+id+'" class="btn btn-danger deleteDayBooking">날짜예약취소</button></td>';
                    }
                    tags += '</tr>';
                }
                $('#usedDatesTbody').html(tags);
            }
        },
        error: function(error) {
            alert(error.responseJSON.message);
            location.href = "/logout";
        }
    });
}