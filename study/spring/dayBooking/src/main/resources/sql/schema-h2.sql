CREATE TABLE User
(
    userId   CHAR(36)       NOT NULL COMMENT '사용자 고유 id',
    signId   VARCHAR(10)    NOT NULL COMMENT '사용자 id',
    username VARCHAR(50)    NOT NULL COMMENT '사용자명',
    password LONGTEXT       NOT NULL COMMENT '비밀번호',
    grade    ENUM('GENERAL', 'ADMIN') DEFAULT 'GENERAL' COMMENT '등급',
    joinDate DATETIME       NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '가입일',
    PRIMARY KEY (userId)
);

CREATE TABLE DayBooing (
    idx             INT(11)         NOT NULL AUTO_INCREMENT COMMENT '날짜 예약 idx',
    userId          CHAR(36)        NOT NULL COMMENT '사용자 id',
    duration        INT(3)           DEFAULT NULL COMMENT '예약 기간',
    startDate       DATETIME        DEFAULT NULL COMMENT '시작일',
    endDate         DATETIME        DEFAULT NULL COMMENT '종료일',
    comment         VARCHAR(300)    DEFAULT NULL COMMENT '코멘트',
    isCanceled      BIT(1)          NULL DEFAULT '0' COMMENT '삭제여부',
    cancelDate      DATETIME        DEFAULT NULL COMMENT '삭제일자',
    regDate         DATETIME        NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT '등록일',
    PRIMARY KEY (idx)
);

