package com.my.transaction.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ErrorStudyService {

    public void tryCatchTest() throws Exception {
        try {
            throw new Exception("에러!");
        } catch (Exception e) {
            log.error("111");
            //e.printStackTrace();
            throw new Exception("ㅇㅔ러에러22");
        } finally {
            log.info("무조건!");
        }
    }

    /**
     * throw는 예외를 호출한곳으로 떠넘기도록 함
     * */
    public void errorInfo() throws Exception {
        throw new Exception("ㅇㅔ러에러11");
    }

    public void throwTest() throws Exception {
        try {
            this.errorInfo();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("ㅇㅔ러에러22");
        } finally {
            log.info("무조건!");
        }
    }
}
