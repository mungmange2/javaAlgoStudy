package com.my.transaction.controller;

import com.my.transaction.service.ErrorStudyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ErrorStudyController {

    private final ErrorStudyService errorStudyService;

    @RequestMapping("/try-catch")
    public void getTryCatchTest() throws Exception {
        this.errorStudyService.tryCatchTest();
    }

    @RequestMapping("/error-info")
    public void errorInfo() throws Exception {
        this.errorStudyService.errorInfo();
    }

    @RequestMapping("/throw-test")
    public void getThrowTest() throws Exception {
        this.errorStudyService.throwTest();
    }
}
