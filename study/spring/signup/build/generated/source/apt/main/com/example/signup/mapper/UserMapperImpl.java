package com.example.signup.mapper;

import com.example.signup.entity.User;
import com.example.signup.service.dto.UserDto;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-09-12T23:01:46+0900",
    comments = "version: 1.3.0.Beta2, compiler: javac, environment: Java 11.0.12 (Amazon.com Inc.)"
)
@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public User toEntity(UserDto dto) {
        if ( dto == null ) {
            return null;
        }

        User user = new User();

        user.setIdx( dto.getIdx() );
        user.setName( dto.getName() );
        user.setEmail( dto.getEmail() );
        user.setPassword( dto.getPassword() );
        user.setCreateDate( dto.getCreateDate() );
        user.setUpdateDate( dto.getUpdateDate() );

        return user;
    }

    @Override
    public UserDto toDto(User entity) {
        if ( entity == null ) {
            return null;
        }

        UserDto userDto = new UserDto();

        userDto.setIdx( entity.getIdx() );
        userDto.setName( entity.getName() );
        userDto.setEmail( entity.getEmail() );
        userDto.setPassword( entity.getPassword() );
        userDto.setCreateDate( entity.getCreateDate() );
        userDto.setUpdateDate( entity.getUpdateDate() );

        return userDto;
    }
}
