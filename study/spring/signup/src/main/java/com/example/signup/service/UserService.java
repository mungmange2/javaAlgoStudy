package com.example.signup.service;

import com.example.signup.entity.User;
import com.example.signup.mapper.UserMapper;
import com.example.signup.repository.UserRepository;
import com.example.signup.service.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    /**
     * 사용자 저장
     * */
    public User createUserByMapper(UserDto userDto) {
        User user = this.userMapper.toEntity(userDto); // dto 객체를 넘겨서 entity 으로 받도록 처리 ?
        return this.userRepository.save(user);
    }
}
