package com.example.signup.controller;

import com.example.signup.entity.User;
import com.example.signup.service.UserService;
import com.example.signup.service.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

@Controller
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("")
    public String viewSignup() {
        return "signup";
    }

    @PostMapping("/signup")
    @ResponseBody
    public String createUserByMapper(@ModelAttribute UserDto userDto, HttpServletResponse response) {
        User user = this.userService.createUserByMapper(userDto);
        return String.format("%s, 회원가입 완료", user.getName());
    }
}
