package com.example.signup.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Setter
@Getter
@Entity
@Table(name="User")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="idx")
    private Long idx;

    @Column(name="name")
    private String name;

    @Column(name="email")
    private String email;

    @Column(name="password")
    private String password;

    @Column(name="create_date")
    private LocalDateTime createDate = LocalDateTime.now();

    @Column(name="update_date")
    private LocalDateTime updateDate;
}
