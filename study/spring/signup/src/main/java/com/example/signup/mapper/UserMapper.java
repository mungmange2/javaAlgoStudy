package com.example.signup.mapper;

import com.example.signup.entity.User;
import com.example.signup.service.dto.UserDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper extends EntityMapper<UserDto, User> {

}
