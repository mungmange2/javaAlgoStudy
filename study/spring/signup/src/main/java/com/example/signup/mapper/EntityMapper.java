package com.example.signup.mapper;

public interface EntityMapper<D, E> {
    E toEntity(D dto); // userDto -> user

    D toDto(E entity); // user -> userDto
}
