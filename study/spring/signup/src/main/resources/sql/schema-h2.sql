DROP TABLE IF EXISTS User;

CREATE TABLE User (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  password VARCHAR(255) NOT NULL,
  create_date DATETIME DEFAULT NULL,
  update_date DATETIME DEFAULT NULL
);

INSERT INTO User (name, password, email) VALUES
('apple', password(1234), 'apple@koko.com'),
('berry', password(1234), 'berry@koko.com'),
('orange', password(1234), 'orange@koko.com');