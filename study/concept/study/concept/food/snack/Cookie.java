package food.snack;

public class Cookie extends Snack {
    private static String cookie = "test111";
    public static void main(String[] args) {
        Snack snack = new Snack();
        System.out.println(cookie); // static 변수
        System.out.println(snack.cookie); // 부모의 변수

        Cookie cookie = new Cookie();
        cookie.test();
        System.out.println(snack.cookie); // cookie.test() 에서 값을 변경 하였다 하여 영향 주진 않음.

        Snack cookie1 = new Cookie();
        System.out.println(cookie1.chocolate); // 부모의 값을 사용 가능!
    }
    public void test() {
        super.cookie = "쿠키";
        System.out.println(super.cookie);
    }
}
