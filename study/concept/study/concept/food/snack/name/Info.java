package food.snack.name;

import food.snack.Snack;

public class Info extends Snack {
    public static void main(String[] args) {
        Info info = new Info();
//        System.out.println(names.candy); private error
        System.out.println(info.cookie);
//        System.out.println(names.jelly); default error
        System.out.println(info.chocolate);
    }
}