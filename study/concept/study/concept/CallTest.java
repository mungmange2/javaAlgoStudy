
public class CallTest {

    public static void main(String[] args) {
        String test1 = "test1";
        String test2 = "test2";
        CallTest.callByValue(test1, test2); // 값에 의한 참조
        System.out.println("test1:" + test1+", test2:"+test2); // 영향 안미침.
    }

    public static void callByValue(String test1, String test2) {
        String temp = test1;
        test1 = test2;
        test2  = temp;
        System.out.println("test1:" + test1+", test2:"+test2);
    }
}
