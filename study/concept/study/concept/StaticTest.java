public class StaticTest {
    private String test = "test";
    private static String test2 = "test2";
    public static void main(String[] args) {
        StaticTest staticTest = new StaticTest();
        staticTest.test = "test1";
        System.out.println("test:" + staticTest.test);
        StaticTest.test2 = "test3"; // static 변수값 변경
        System.out.println("test2:" + StaticTest.test2);
        StaticTest.test2 = "test4"; // static 변수값 변경
        staticTest.testMethod(staticTest);
    }

    public void testMethod(StaticTest staticTest) {
        System.out.println("[testMethod] test:" + staticTest.test);
        System.out.println("[testMethod] test2:" + staticTest.test2);

        StaticTest staticTest2 = new StaticTest();
        System.out.println("[testMethod] test:" + staticTest2.test); // 초기화 된 인스턴스 변수 값을 보여줌.
        System.out.println("[testMethod] test2:" + staticTest2.test2); // 클래스 변수에 저장된 값을 보여줌. (static 공유)
    }
}
