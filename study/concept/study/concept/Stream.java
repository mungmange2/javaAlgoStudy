package study.concept;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Stream {
    public static void main(String[] args) {
        StreamDto streamDto = Stream.settingStreamDto();

        // forEach
//        streamDto.getStreamInfos().stream().forEach(vo -> {
//            System.out.println(vo.id);
//            System.out.println(vo.content);
//            System.out.println(vo.isDelete);
//            System.out.println(vo.createDate);
//        });

        // filter
//        List<StreamDto.StreamInfo> streamInfos
//                = streamDto.getStreamInfos().stream()
//                .filter(vo -> vo.isDelete)
//                .collect(Collectors.toList());

//        streamInfos.stream().forEach(vo -> {
//            System.out.println(vo.id);
//            System.out.println(vo.content);
//            System.out.println(vo.isDelete);
//            System.out.println(vo.createDate);
//        });

        // findFirst, orElse
//        StreamDto.StreamInfo streamInfo
//                = streamDto.getStreamInfos().stream()
//                .filter(vo -> vo.isDelete) // delete 가 false
//                .findFirst() // 첫번째 데이터 추출
//                .orElse(null); // 없으면 null
//        System.out.println(streamInfo.id);
//        System.out.println(streamInfo.content);

        // map
        List<Integer> streamInfos = streamDto.getStreamInfos().stream().map(vo -> vo.id).collect(Collectors.toList());
        for (Integer integer : streamInfos) {
            System.out.println(integer);
        }

        // https://ryan-han.com/post/java/java-stream/ 참고함
    }

    public static class StreamDto {

        private List<StreamInfo> streamInfos;

        public StreamDto setStreamInfos(List<StreamInfo> streamInfos) {
            this.streamInfos = streamInfos;
            return this;
        }
        public List<StreamInfo> getStreamInfos() {
            return streamInfos;
        }

        public StreamDto build() {
            return new StreamDto();
        }

        private static class StreamInfo {
            private Integer id;
            private String content;
            private Boolean isDelete;
            private LocalDateTime createDate;

            public StreamInfo setId(Integer id) {
                this.id = id;
                return this;
            }

            public StreamInfo setContent(String content) {
                this.content = content;
                return this;
            }

            public StreamInfo setDelete(Boolean delete) {
                isDelete = delete;
                return this;
            }

            public StreamInfo setCreateDate(LocalDateTime createDate) {
                this.createDate = createDate;
                return this;
            }
            
            public StreamInfo build() {
                return new StreamInfo();
            }
        }
    }

    public static StreamDto settingStreamDto() {

        List<StreamDto.StreamInfo> streamInfos = new ArrayList<>();
        StreamDto.StreamInfo streamInfo = new StreamDto.StreamInfo().build()
                .setId(1)
                .setContent("안녕하세요")
                .setDelete(false)
                .setCreateDate(LocalDateTime.now());
        streamInfos.add(streamInfo);

        StreamDto.StreamInfo streamInfo2 = new StreamDto.StreamInfo().build()
                .setId(2)
                .setContent("2안녕하세요")
                .setDelete(true)
                .setCreateDate(LocalDateTime.now());
        streamInfos.add(streamInfo2);

        StreamDto.StreamInfo streamInfo3 = new StreamDto.StreamInfo().build()
                .setId(3)
                .setContent("3안녕하세요")
                .setDelete(false)
                .setCreateDate(LocalDateTime.now());
        streamInfos.add(streamInfo3);


        StreamDto streamDto = new StreamDto().build()
                .setStreamInfos(streamInfos);
        return streamDto;
    }
}
